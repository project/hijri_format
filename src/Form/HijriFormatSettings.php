<?php

namespace Drupal\hijri_format\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class settings.
 */
class HijriFormatSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'adjustment' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hijri_format.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hijri_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hijri_format.settings');
    $form['adjustment'] = [
      '#type' => 'number',
      '#title' => $this->t('Hijri Date Adjustment'),
      '#description' => t('You may need this option if you are using Umm Ulqura'),
      '#default_value' => $config->get('adjustment'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('hijri_format.settings')
      ->set('adjustment', $form_state->getValue('adjustment'))
      ->save();
  }

}
