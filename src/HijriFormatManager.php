<?php

namespace Drupal\hijri_format;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use phpHijri\Calendar;

/**
 * Class HijriFormatManager.
 */
class HijriFormatManager {

  /**
   * Date Formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */

  private $dateFormatter;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Indian numbers.
   *
   * @var array
   */
  protected $indianNumbers = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];

  /**
   * Arabic numbers.
   *
   * @var array
   */
  protected $arabicNumbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  /**
   * Constructs HijriFormatManager.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(DateFormatter $dateFormatter, ConfigFactoryInterface $configFactory) {
    $this->dateFormatter = $dateFormatter;
    $this->configFactory = $configFactory;
  }

  /**
   * Builds hijri formatter settings.
   *
   * @return array
   *   Form render array.
   */
  public function buildForm($settings) {
    $form['format'] = [
      '#title' => t('format'),
      '#type' => 'textfield',
      '#description' => t('Format of the hijri date, [ default d-M-Y ]'),
      '#default_value' => $settings["format"],
      '#required' => TRUE,
    ];

    $form['is_indian'] = [
      '#title' => t('Use Indian numbers'),
      '#type' => 'checkbox',
      '#description' => t('Will be applied when language is Arabic'),
      '#default_value' => $settings["is_indian"],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  private function escapeString($string) {
    // Escape everything.
    $escaped_string = '';
    preg_match_all('/./', $string, $matches);
    foreach ($matches[0] as $character) {
      $escaped_string .= '\\' . $character;
    }
    return $escaped_string;
  }

  /**
   * {@inheritdoc}
   *
   * @param string $month
   *   Month number.
   *
   * @return string
   *   The months names is taken from http://www.ummulqura.org.sa/
   */
  private function getStandardMonthName($month) {
    $months = [
      1 => t('Muharram'),
      2 => t('Safar'),
      3 => t('Rabi’ Al-Awwal'),
      4 => t('Rabi’ Al-Thani'),
      5 => t('Jumada Al-Ula'),
      6 => t('Jumada Al-Alkhirah'),
      7 => t('Rajab'),
      8 => t('Sha’ban'),
      9 => t('Ramadan'),
      10 => t('Shawwal'),
      11 => t('Thul-Qi’dah'),
      12 => t('Thul-Hijjah'),
    ];
    return $this->escapeString($months[$month]);
  }

  /**
   * {@inheritdoc}
   *
   * @param string $timestamp
   *   Date in timestamp.
   * @param string $format
   *   Date format.
   * @param int $is_indian
   *   If 1, return the converted indian date.
   */
  public function convertToHijri($timestamp, $format, $is_indian = 0) {

    // Set Adjustment.
    $adjustment = $this->configFactory->get('hijri_format.settings')->get('adjustment');

    // Get Hijri date.
    $year = date('Y', $timestamp);
    $month = date('m', $timestamp);
    $day = date('d', $timestamp);
    $calender = new Calendar();
    $hijri = $calender->gregorianToHijri($year, $month, $day);
    // Add adjusment to hijri date.
    $hijri['d'] = $hijri['d'] + $adjustment;
    $date_format = $hijri['d'] . "-" . $hijri['m'] . "-" . $hijri['y'];
    $date_format = date_create($date_format);

    // Replace supported formats with Hijri dates,
    // this idea come to life to render dates through Drupal
    // So, the translation will be a matter of interface translation.
    $patterns = [
      0 => '/(?<!\\\\)d/',
      1 => '/(?<!\\\\)j/',
      2 => '/(?<!\\\\)z/',
      3 => '/(?<!\\\\)W/',
      4 => '/(?<!\\\\)F/',
      5 => '/(?<!\\\\)m/',
      6 => '/(?<!\\\\)M/',
      7 => '/(?<!\\\\)n/',
      8 => '/(?<!\\\\)t/',
      9 => '/(?<!\\\\)L/',
      10 => '/(?<!\\\\)o/',
      11 => '/(?<!\\\\)Y/',
      12 => '/(?<!\\\\)y/',
    ];

    $replacements = [
      0 => date_format($date_format, "d"),
      1 => date_format($date_format, "j"),
      2 => '',
      3 => '',
      4 => $this->getStandardMonthName(date_format($date_format, "n")),
      5 => date_format($date_format, "m"),
      6 => $this->getStandardMonthName(date_format($date_format, "n")),
      7 => date_format($date_format, "n"),
      8 => '',
      9 => '',
      10 => date_format($date_format, "o"),
      11 => date_format($date_format, "Y"),
      12 => date_format($date_format, "y"),
    ];

    $format = preg_replace($patterns, $replacements, $format);

    $date = $this->dateFormatter->format($timestamp, 'custom', $format);

    return ($is_indian) ? $this->convertIndian($date) : $date;
  }

  /**
   * {@inheritdoc}
   */
  public function convertIndian($dateString) {
    return str_replace($this->arabicNumbers, $this->indianNumbers, $dateString);
  }

}
