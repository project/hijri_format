<?php

namespace Drupal\hijri_format\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\hijri_format\HijriFormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'hijri_dual_date_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hijri_dual_date_formatter",
 *   label = @Translation("Hijri Dual Date Formatter"),
 *   field_types = {
 *     "timestamp",
 *     "datetime",
 *     "daterange",
 *     "created",
 *     "changed"
 *   }
 * )
 */
class HijriDualDateFormatter extends FormatterBase {

  /**
   * Hijri manager.
   *
   * @var \Drupal\hijri_format\HijriFormatManager
   */
  protected $hijriFormatManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, HijriFormatManager $hijri_format_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->hijriFormatManager = $hijri_format_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('hijri_format.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format' => 'd-M-Y',
      'separator' => 'To',
      'is_indian' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date Format'),
      '#options' => [
        'd-M-Y' => 'd-M-Y',
        'M-d-Y' => 'M-d-Y',
        'Y-M-d' => 'Y-M-d',
      ],
      '#default_value' => $this->getSetting('format'),
    ];

    $elements['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#default_value' => $this->getSetting('separator'),
    ];

    $elements['is_indian'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Indian Hijri Calendar'),
      '#default_value' => $this->getSetting('is_indian'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Format: @format', ['@format' => $this->getSetting('format')]);
    $summary[] = $this->t('Separator: @separator', ['@separator' => $this->getSetting('separator')]);
    $summary[] = $this->t('Indian Calendar: @is_indian', ['@is_indian' => $this->getSetting('is_indian') ? 'Yes' : 'No']);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      try {
        $format = $this->getSetting('format');
        $is_indian = (bool) $this->getSetting('is_indian');
        // Handle different types of date values.
        $date_value = $item->getValue();

        // Determine the type of value and convert to DateTime.
        if (is_numeric($date_value['value'])) {
          // If it's a timestamp, create DateTime from timestamp.
          $date = \DateTime::createFromFormat('U', $date_value['value'], new \DateTimeZone('UTC'));
        }
        else {
          // If it's a date string, create DateTime directly.
          $date = new \DateTime($date_value['value'], new \DateTimeZone('UTC'));
        }

        // Ensure date is valid.
        if (!$date) {
          \Drupal::logger('hijri_dual_date_formatter')->warning(
            'Unable to parse date value: @value',
            ['@value' => print_r($date_value['value'], TRUE)]
          );
          continue;
        }

        // Gregorian date formatting.
        $gregorian_date = $date->format('d F Y');

        // Hijri date conversion.
        $hijri_date = $this->hijriFormatManager->convertToHijri($date_value['value'], $format, $is_indian);

        // Render the dual date.
        $elements[$delta] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['hijri-dual-date-formatter'],
          ],
          'hijri' => [
            '#markup' => '<span class="hijri-date" dir="rtl">' . $hijri_date . '</span>',
          ],
          'separator' => [
            '#markup' => '<span class="date-separator">' . $this->getSetting('separator') . '</span>',
          ],
          'gregorian' => [
            '#markup' => '<span class="gregorian-date">' . $gregorian_date . '</span>',
          ],
        ];
      }
      catch (\Exception $e) {
        // Log any errors.
        \Drupal::logger('hijri_dual_date_formatter')->error(
          'Error formatting date: @error (Value: @value)',
          [
            '@error' => $e->getMessage(),
            '@value' => print_r($date_value['value'] ?? 'N/A', TRUE),
          ]
              );
      }
    }

    return $elements;
  }

}
