<?php

namespace Drupal\hijri_format\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\hijri_format\HijriFormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'hijri_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hijri_field_formatter",
 *   label = @Translation("Hijri"),
 *   field_types = {
 *     "datetime",
 *     "daterange",
 *     "created",
 *     "changed"
 *   }
 * )
 */
class HijriFieldFormatter extends FormatterBase {

  /**
   * Hijri manager.
   *
   * @var \Drupal\hijri_format\HijriFormatManager
   */
  protected $hijriFormatManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, HijriFormatManager $hijri_format_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->hijriFormatManager = $hijri_format_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('hijri_format.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
      'format' => 'd-M-Y',
      'is_indian' => 0,
      'separator' => 'To',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    $form = $this->hijriFormatManager->buildForm($settings);

    if ($this->fieldDefinition->getType() === "daterange") {
      $form['separator'] = [
        '#title' => 'Separator',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting("separator"),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $is_indian = ($settings['is_indian'] === 1) ? t("Yes") : t("No");

    $summary[] = t('Format:') . ' ' . $settings['format'];
    $summary[] = t('Indian Numbers:') . ' ' . $is_indian;

    if ($this->fieldDefinition->getType() === "daterange") {
      $summary[] = t('Separator:') . ' ' . $this->getSetting('separator');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $date = [];
    foreach ($items as $delta => $item) {
      $format = $this->getSetting('format');
      $is_indian = (bool) $this->getSetting('is_indian');
      $separator = $this->getSetting('separator');
      $class = get_class($item);
      switch ($class) {
        case DateRangeItem::class:
          $date[] = $this->hijriFormatManager->convertToHijri($this->getUtcTimestamp($item->getValue()['value']), $format, $is_indian);
          $date[] = $this->hijriFormatManager->convertToHijri($this->getUtcTimestamp($item->getValue()['end_value']), $format, $is_indian);
          break;

        case DateTimeItem::class:
          $value = $this->getUtcTimestamp($item->getValue()['value']);
        default:
          $value = $value ?? $item->getValue()['value'];
          $date[] = $this->hijriFormatManager->convertToHijri($value, $format, $is_indian);
      }
      $elements[$delta] = [
        '#theme' => 'hijri',
        '#date' => $date,
        '#separator' => $separator,
      ];
    }
    return $elements;
  }

  /**
   * Gets the timestamp using UTC timezone.
   *
   * @param string $date_time_string
   *   DateTime string (e.g. 1996-04-24T12:00:00).
   *
   * @return int
   *   Timestamp using UTC timezone.
   */
  private function getUtcTimestamp(string $date_time_string) {
    $timezone = new \DateTimeZone('UTC');
    $datetime_type = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('datetime_type');
    $date_time_string = str_replace("T", " ", $date_time_string);
    $format = $datetime_type === 'date' ? 'Y-m-d' : 'Y-m-d H:i:s';
    return \DateTime::createFromFormat($format, $date_time_string, $timezone)->getTimestamp();
  }

}
