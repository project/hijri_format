<?php

namespace Drupal\hijri_format\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\hijri_format\HijriFormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Hijri' block.
 *
 * @Block(
 *  id = "hijri_block",
 *  admin_label = @Translation("Hijri Current Date Block"),
 * )
 */
class HijriBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Hijri Manager.
   *
   * @var hijriFormatManager
   */
  protected $hijriFormatManager;

  /**
   * Constructs a new HijriDate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param hijriFormatManager $hijri_manager
   *   Hijri manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, hijriFormatManager $hijri_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->hijriFormatManager = $hijri_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hijri_format.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'format' => 'd-M-Y',
      'is_indian' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return $this->hijriFormatManager->buildForm($this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['format'] = $form_state->getValue('format');
    $this->configuration['is_indian'] = $form_state->getValue('is_indian');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $format = $this->configuration['format'];
    $is_indian = $this->configuration['is_indian'];
    $date = $this->hijriFormatManager->convertToHijri(time(), $format, $is_indian);
    $build['#markup'] = $date;
    return $build;
  }

}
